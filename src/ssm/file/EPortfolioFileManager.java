package ssm.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import static ssm.StartupConstants.PATH_SLIDE_SHOWS;
import ssm.components.BannerImageComponent;
import ssm.components.Component;
import ssm.components.FooterComponent;
import ssm.components.HeadingComponent;
import ssm.components.ImageComponent;
import ssm.components.ParagraphComponent;
import ssm.components.StudentNameComponent;
import ssm.components.VideoComponent;
import ssm.model.Page;
import ssm.model.EPortfolioModel;

/**
 * This class uses the JSON standard to read and write slideshow data files.
 *
 * @author McKilla Gorilla & _____________
 */
public class EPortfolioFileManager {

    // JSON FILE READING AND WRITING CONSTANTS
    public static String JSON_TITLE = "title";
    public static String JSON_PAGES = "pages";
    public static String JSON_IMAGE_FILE_NAME = "image_file_name";
    public static String JSON_IMAGE_PATH = "image_path";
    public static String JSON_CAPTION = "caption";
    public static String JSON_EXT = ".json";
    public static String SLASH = "/";

    /**
     * This method saves all the data associated with a slide show to a JSON
     * file.
     *
     * @param ePortfolioToSave
     *
     * @throws IOException Thrown when there are issues writing to the JSON
     * file.
     */
    public void saveEPortfolio(EPortfolioModel ePortfolioToSave) throws IOException {
        StringWriter sw = new StringWriter();

        // BUILD THE SLIDES ARRAY
        JsonArray slidesJsonArray = makePagesJsonArray(ePortfolioToSave.getPages());

        // NOW BUILD THE COURSE USING EVERYTHING WE'VE ALREADY MADE
        JsonObject ePortfolioJsonObject = Json.createObjectBuilder()
                .add(JSON_PAGES, slidesJsonArray)
                .build();

        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);

        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(ePortfolioJsonObject);
        jsonWriter.close();

        // INIT THE WRITER
        String ePortfolioTitle = "" + "EPortfolio";
        String jsonFilePath = PATH_SLIDE_SHOWS + SLASH + ePortfolioTitle + JSON_EXT;
        OutputStream os = new FileOutputStream(jsonFilePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(ePortfolioJsonObject);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(jsonFilePath);
        pw.write(prettyPrinted);
        pw.close();
        System.out.println(prettyPrinted);
    }

    /**
     * This method loads the contents of a JSON file representing a slide show
     * into a SlideSShowModel object.
     *
     * @param ePortfolioToLoad The slide show to load
     * @param jsonFilePath The JSON file to load.
     * @throws IOException
     */
    public void loadEPortfolio(EPortfolioModel ePortfolioToLoad, String jsonFilePath) throws IOException {
        // LOAD THE JSON FILE WITH ALL THE DATA
        JsonObject json = loadJSONFile(jsonFilePath);

        // NOW LOAD THE COURSE
        ePortfolioToLoad.reset();
        //ePortfolioToLoad.setTitle(json.getString(JSON_TITLE));
        JsonArray jsonPagesArray = json.getJsonArray(JSON_PAGES);
        for (int i = 0; i < jsonPagesArray.size(); i++) {
            String pageTitle = jsonPagesArray.getString(i);
            JsonObject pageJso = jsonPagesArray.getJsonObject(i + 1);
            i++;
            ePortfolioToLoad.addTab(pageTitle);
            JsonArray jsonComponentsArray = pageJso.getJsonArray("components");
            for (int j = 0; j < jsonComponentsArray.size(); j++) {
                JsonObject componentJso = jsonComponentsArray.getJsonObject(j);
                String type = componentJso.getString("component_type");
                if (type.equals("Image")) //System.out.println("bro we're getting sowmewhere");
                {
                 ImageComponent imgComp = new ImageComponent(ePortfolioToLoad.getUi(),ePortfolioToLoad.getPages().get(i/2)); 
                 imgComp.loadComponentJsonObject(componentJso);
                }
                if (type.equals("Video")) //System.out.println("bro we're getting sowmewhere");
                {
                 VideoComponent vidComp = new VideoComponent(ePortfolioToLoad.getPages().get(i/2),ePortfolioToLoad.getUi()); 
                 vidComp.loadComponentJsonObject(componentJso);
                }
               if (type.equals("Heading")) //System.out.println("bro we're getting sowmewhere");
                {
                 HeadingComponent headComp = new HeadingComponent(ePortfolioToLoad.getPages().get(i/2),ePortfolioToLoad.getUi()); 
                 headComp.loadComponentJsonObject(componentJso);
                }
                if (type.equals("Paragraph")) //System.out.println("bro we're getting sowmewhere");
                {
                 ParagraphComponent paraComp = new ParagraphComponent(ePortfolioToLoad.getPages().get(i/2),ePortfolioToLoad.getUi()); 
                 paraComp.loadComponentJsonObject(componentJso);
                }
                 if (type.equals("BannerImage")) //System.out.println("bro we're getting sowmewhere");
                {
                 BannerImageComponent bannComp = new BannerImageComponent(ePortfolioToLoad.getUi(),ePortfolioToLoad.getPages().get(i/2)); 
                 bannComp.loadComponentJsonObject(componentJso);
                }
                else if (type.equals("StudentName")) //System.out.println("bro we're getting sowmewhere");
                {
                 StudentNameComponent stuComp = new StudentNameComponent(ePortfolioToLoad.getUi(),ePortfolioToLoad.getPages().get(i/2)); 
                 stuComp.loadComponentJsonObject(componentJso);
                }
                if (type.equals("Footer")) //System.out.println("bro we're getting sowmewhere");
                {
                 FooterComponent footComp = new FooterComponent(ePortfolioToLoad.getUi(),ePortfolioToLoad.getPages().get(i/2)); 
                 footComp.loadComponentJsonObject(componentJso);
                }
                
            }
            ePortfolioToLoad.getUi().reloadTabsPane();
            // pageJso.getString(SLASH)

        }
    }

    // AND HERE ARE THE PRIVATE HELPER METHODS TO HELP THE PUBLIC ONES
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }

    private ArrayList<String> loadArrayFromJSONFile(String jsonFilePath, String arrayName) throws IOException {
        JsonObject json = loadJSONFile(jsonFilePath);
        ArrayList<String> items = new ArrayList();
        JsonArray jsonArray = json.getJsonArray(arrayName);
        for (JsonValue jsV : jsonArray) {
            items.add(jsV.toString());
        }
        return items;
    }

    private JsonObject makeComponentsJsonObject(List<Component> components, Page page) {
        //JsonArray slidesJsonArray = makeComponentsJsonArray(ePortfolioToSave.getPages());
        JsonArray componentsJsonArray = makeComponentsJsonArray(components, page);
        JsonObject componentsJsonObject = Json.createObjectBuilder()
                .add("components", componentsJsonArray)
                .build();
        return componentsJsonObject;
    }

    private JsonArray makePagesJsonArray(List<Page> pages) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Page page : pages) {
            jsb.add(page.getPageTitle());
            JsonObject jobj = makeComponentsJsonObject(page.getComponents(), page);
            jsb.add(jobj);
        }
        JsonArray jA = jsb.build();
        return jA;
    }

    private JsonArray makeComponentsJsonArray(List<Component> components, Page page) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Component component : components) {
            JsonObject jso = component.makeComponentJsonObject();
            jsb.add(jso);
        }
        if(page.getStudentName()!=null){
            JsonObject jso = page.getStudentName().makeComponentJsonObject();
            jsb.add(jso);
        }
        if(page.getBannerImage()!=null){
            JsonObject jso = page.getBannerImage().makeComponentJsonObject();
            jsb.add(jso);
        }
        if(page.getFooter()!=null){
            JsonObject jso = page.getFooter().makeComponentJsonObject();
            jsb.add(jso);
        }
        JsonArray jA = jsb.build();
        return jA;
    }

//    private JsonObject makeComponentJsonObject(Component component) {
//	JsonObject jso = Json.createObjectBuilder()
//		.add(JSON_IMAGE_FILE_NAME, slide.getImageFileName())
//		.add(JSON_IMAGE_PATH, slide.getImagePath())
//		.add(JSON_CAPTION, slide.getCaption())
//		.build();
//	return jso;
//    }
}
