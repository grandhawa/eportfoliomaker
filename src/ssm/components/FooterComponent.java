/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssm.components;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javax.json.Json;
import javax.json.JsonObject;
import static ssm.StartupConstants.CSS_CLASS_LANG_COMBO_BOX;
import static ssm.StartupConstants.CSS_CLASS_LANG_DIALOG_PANE;
import static ssm.StartupConstants.CSS_CLASS_LANG_OK_BUTTON;
import static ssm.StartupConstants.CSS_CLASS_LANG_PROMPT;
import static ssm.StartupConstants.STYLE_SHEET_UI;
import ssm.model.Page;
import ssm.view.EPortfolioMakerView;

/**
 *
 * @author Randhawa
 */
public class FooterComponent extends Component{
    Page page;
    Stage stage;
    Label footerLabel;
    Scene scn;
    GridPane gridPane;
    TextField footerField;
    Button okButton;
    Button cancelButton;
    int index = -1;
    Label footer;
    EPortfolioMakerView ui;

    public FooterComponent(EPortfolioMakerView initUi,Page initPage) {
        super(initPage);
        ui = initUi;
        footer = new Label();
        page = initPage;
        stage = new Stage();
        gridPane = new GridPane();
        footerLabel = new Label();
        footerLabel.setText("Enter Footer: ");
        footerField = new TextField();
        okButton = new Button("OK");
        cancelButton = new Button("Cancel");
        
        gridPane.add(footerLabel, 0,0,2,1);
        gridPane.add(footerField, 0, 1, 2, 1);
        gridPane.add(okButton, 0, 2, 1,1);
        gridPane.add(cancelButton, 1, 2, 1,1);
        
        Scene scn = new Scene(gridPane);
        footerLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        gridPane.getStyleClass().add(CSS_CLASS_LANG_DIALOG_PANE);
        scn.getStylesheets().add(STYLE_SHEET_UI);
        okButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        cancelButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        stage.setScene(scn);
        eventHandlers(stage);
    }

    public Stage getStage() {
        return stage;
    }
    
     @Override
    public void editComponent() {
        stage.showAndWait();
    }
    
    @Override
     public JsonObject makeComponentJsonObject() {
	JsonObject jso = Json.createObjectBuilder()
                .add("component_type", "Footer")
		.add("footer_text", footerField.getText())
		.build();
	return jso;
    }
     public void loadComponentJsonObject(JsonObject jso) {
        String paragraphText = jso.getString("footer_text");
        footer.setText(paragraphText);
        this.getChildren().add(footer);
        page.setFooter(this);
	//ui.reloadTabsPane();
    }
    
    public void eventHandlers(Stage stage) {

        okButton.setOnAction(e -> {
            if (page.getComponents().contains(this)) {
                index = page.getComponents().indexOf(this);
                page.getComponents().remove(index);
                this.getChildren().clear();
            }
            
            //floatSelection = floatComboBox.getValue().toString();
            //String path = selectedFile.getPath();
            String path = "file:/Users/Randhawa/Documents/cse 219/EPortfolioMaker/SlideShowMaker/images/icons/video.jpg";
            System.out.println(path);
           
            //get index of current page
            //int index = ui.getPageEditorPane().getSelectionModel().getSelectedIndex();
            footer.setText(footerField.getText());
            footer.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
            
            //put the image in the vBox of that box
            this.getChildren().add(footer);
            //if the value of index has been changed
          
                page.setFooter(this);
            

            ui.reloadTabsPane();
            stage.hide();
        });
        cancelButton.setOnAction(e ->{
            stage.hide();
        });
    }

    
    
}
