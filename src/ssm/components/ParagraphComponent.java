/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssm.components;

import java.util.Optional;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.IndexRange;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;
import javax.json.Json;
import javax.json.JsonObject;
import static ssm.StartupConstants.CSS_CLASS_LANG_COMBO_BOX;
import static ssm.StartupConstants.CSS_CLASS_LANG_DIALOG_PANE;
import static ssm.StartupConstants.CSS_CLASS_LANG_OK_BUTTON;
import static ssm.StartupConstants.CSS_CLASS_LANG_PROMPT;
import static ssm.StartupConstants.STYLE_SHEET_UI;
import ssm.model.Page;
import ssm.view.EPortfolioMakerView;

/**
 *
 * @author Randhawa
 */
public class ParagraphComponent extends Component {

    Page page;
    Stage stage;
    Label headerLabel;
    Scene scn;
    GridPane gridPane;
    TextArea paragraphField;
    Button okButton;
    Button cancelButton;
    Button setHyperlink;
    int index = -1;
    Text paragraph;
    EPortfolioMakerView ui;
    TextFlow textFlow;

    public ParagraphComponent(Page initPage, EPortfolioMakerView initUi) {
        super(initPage);
        ui = initUi;
        paragraph = new Text();
        page = initPage;
        stage = new Stage();
        gridPane = new GridPane();
        headerLabel = new Label();
        headerLabel.setText("Enter Paragraph");
        paragraphField = new TextArea();
        okButton = new Button("OK");
        cancelButton = new Button("Cancel");
        setHyperlink = new Button("Set Hyperlink");

        gridPane.add(headerLabel, 0, 0, 2, 1);
        gridPane.add(paragraphField, 0, 1, 2, 1);
        gridPane.add(okButton, 0, 2, 1, 1);
        gridPane.add(cancelButton, 1, 2, 1, 1);
        gridPane.add(setHyperlink, 3, 2, 1, 1);

        Scene scn = new Scene(gridPane);
        headerLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        gridPane.getStyleClass().add(CSS_CLASS_LANG_DIALOG_PANE);
        scn.getStylesheets().add(STYLE_SHEET_UI);
        okButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        cancelButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        setHyperlink.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        stage.setScene(scn);
        eventHandlers(stage);
    }

    public Stage getStage() {
        return stage;
    }

    @Override
    public void editComponent() {
        stage.showAndWait();
    }
    
    @Override
     public JsonObject makeComponentJsonObject() {
	JsonObject jso = Json.createObjectBuilder()
                .add("component_type", "Paragraph")
		.add("paragraph_text", paragraphField.getText())
		.build();
	return jso;
    }
     
      public void loadComponentJsonObject(JsonObject jso) {
        String paragraphText = jso.getString("paragraph_text");
        paragraph.setText(paragraphText);
        this.getChildren().add(paragraph);
        page.getComponents().add(this);
	//ui.reloadTabsPane();
    }

    public void eventHandlers(Stage stage) {

        okButton.setOnAction(e -> {
            if (page.getComponents().contains(this)) {
                index = page.getComponents().indexOf(this);
                page.getComponents().remove(index);
                this.getChildren().clear();
            }

            //floatSelection = floatComboBox.getValue().toString();
            //String path = selectedFile.getPath();
            String path = "file:/Users/Randhawa/Documents/cse 219/EPortfolioMaker/SlideShowMaker/images/icons/video.jpg";
            System.out.println(path);

            //get index of current page
            //int index = ui.getPageEditorPane().getSelectionModel().getSelectedIndex();
            paragraph.setText(paragraphField.getText());

            //put the image in the vBox of that box
            if (textFlow != null) {
                this.getChildren().add(textFlow);
            } else {
                this.getChildren().add(paragraph);
            }
            //if the value of index has been changed
            if (index != -1) {
                page.getComponents().add(index, this);
            } else {
                page.getComponents().add(this);
            }

            ui.reloadTabsPane();
            stage.hide();
        });
        cancelButton.setOnAction(e -> {
            stage.hide();
        });

        setHyperlink.setOnAction(e -> {
            IndexRange indexRange = new IndexRange(paragraphField.getSelection());
            String paragraphText = paragraphField.getText();

            TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Hyperlink");
        dialog.setHeaderText("URL");
        dialog.setContentText("Enter a url for new hyperlink:");

        // get the response value.
        Optional<String> result = dialog.showAndWait();
        String url = "";
        if (result.isPresent()) {
            url = result.get();
        }
            String str1 = paragraphText.substring(0, indexRange.getStart());
            Text text1 = new Text(str1);
            System.out.println("String 1 :" + str1);
            String str2 = paragraphText.substring(indexRange.getEnd());
            Text text2 = new Text(str2);
            System.out.println("String 2 :" + str2);
            Hyperlink hyperlink = new Hyperlink(paragraphField.getSelectedText());
            System.out.println("hyperlink: " + hyperlink.getText());
            Text urlText = new Text("\nURL to be visited: "+url);
            textFlow = new TextFlow(text1, hyperlink, text2, urlText);

        });
    }
    
    
      
}
