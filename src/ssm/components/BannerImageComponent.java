/*
 * To change this license banner, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssm.components;

import java.io.File;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.json.Json;
import javax.json.JsonObject;
import static ssm.StartupConstants.CSS_CLASS_LANG_DIALOG_PANE;
import static ssm.StartupConstants.CSS_CLASS_LANG_OK_BUTTON;
import static ssm.StartupConstants.CSS_CLASS_LANG_PROMPT;
import static ssm.StartupConstants.STYLE_SHEET_UI;
import ssm.model.Page;
import ssm.view.EPortfolioMakerView;

/**
 *
 * @author Randhawa
 */
public class BannerImageComponent extends Component {
    Stage stage;
     Label bannerLabel;
    Scene scn;
    GridPane gridPane;
    Button browseButton;
    Label fileLabel;
    Button okButton;
    Button cancelButton;
    Page page;
    File selectedFile;
    FileChooser fileChooser;
    EPortfolioMakerView ui;

    public BannerImageComponent(EPortfolioMakerView initUi,Page initPage) {
        super(initPage);
        ui = initUi;
        page = initPage;
        stage = new Stage();
        gridPane = new GridPane();
        bannerLabel = new Label();
        bannerLabel.setText("Select Banner Image");
        browseButton = new Button("Browse");
        fileChooser = new FileChooser();
        fileLabel = new Label();
        okButton = new Button("OK");
        cancelButton = new Button("Cancel");
        
        gridPane.add(bannerLabel, 0,0,2,1);
        gridPane.add(browseButton, 0, 1, 1, 1);
        gridPane.add(fileLabel, 1, 1, 1, 1);
        gridPane.add(okButton, 0, 2, 1,1);
        gridPane.add(cancelButton, 1, 2, 1,1);
        
        Scene scn = new Scene(gridPane);
        bannerLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        gridPane.getStyleClass().add(CSS_CLASS_LANG_DIALOG_PANE);
        scn.getStylesheets().add(STYLE_SHEET_UI);
        okButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        cancelButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        stage.setScene(scn);
        eventHandlers(stage);
    }
    
     public void eventHandlers(Stage stage) {
        browseButton.setOnAction(e -> {
            selectedFile = fileChooser.showOpenDialog(stage);
            if (selectedFile != null) {
                fileLabel.setText(selectedFile.getName());
                System.out.println(selectedFile.getName());
            }
            else
                System.out.println("File is null");
        });
        
        okButton.setOnAction(e -> {
            int imageHeight = 200;
            int imageWidth = 600;
            String path = selectedFile.getPath();
            setImageComponent(imageHeight, imageWidth, path);
            //if the value of index has been changed
            page.setBannerImage(this);

            ui.reloadTabsPane();
            stage.hide();
        });
        cancelButton.setOnAction(e -> {
            stage.hide();
        });
        
    }
     
     @Override
    public void editComponent() {
        stage.showAndWait();
    }

    /**
     *
     * @return
     */
    @Override
     public JsonObject makeComponentJsonObject() {
	JsonObject jso = Json.createObjectBuilder()
                .add("component_type", "BannerImage")
		.add("image_path", selectedFile.getPath())
		.build();
	return jso;
    }
     
      public void loadComponentJsonObject(JsonObject jso) {
        String path = jso.getString("image_path");
        setImageComponent(200, 600, path);
        //page.getComponents().add(this);
        page.setBannerImage(this);

	//ui.reloadTabsPane();
    }

     
     public void setImageComponent(int initImageHeight, int initImageWidth,  String initpath){
        
            initpath = "file:" + initpath;
            System.out.println(initpath);
            Image img = new Image(initpath);
            ImageView imgView = new ImageView(img);
            imgView.setFitHeight(initImageHeight);
            imgView.setFitWidth(initImageWidth);
            this.getChildren().add(imgView);
    }

    public Stage getStage() {
        return stage;
    }
     
}
