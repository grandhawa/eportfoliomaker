/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssm.components;

import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javax.json.Json;
import javax.json.JsonObject;
import ssm.model.Page;

/**
 *
 * @author Randhawa
 */
public class Component extends VBox{
    private boolean selected;
    public Component(Page page){
        selected = false;
        this.setOnMousePressed(e -> {
	   page.processComponentSelection(this);
	});
    }
    public boolean isSelected(){
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
    
    public void editComponent(){
    
    }
    
    public JsonObject makeComponentJsonObject() {
	JsonObject jso = Json.createObjectBuilder()
//		.add(JSON_IMAGE_FILE_NAME, selectedFile.getName())
//		.add(JSON_IMAGE_PATH, selectedFile.getPath())
//		.add(JSON_CAPTION, captionField.getText())
//                .add("image_height", heightField.getText())
//                .add("image_width", widthField.getText())
//                .add("float_selection", floatSelection)
		.build();
	return jso;
    }
    
}

