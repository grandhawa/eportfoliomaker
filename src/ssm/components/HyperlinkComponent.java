/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssm.components;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import static ssm.StartupConstants.CSS_CLASS_LANG_COMBO_BOX;
import static ssm.StartupConstants.CSS_CLASS_LANG_DIALOG_PANE;
import static ssm.StartupConstants.CSS_CLASS_LANG_OK_BUTTON;
import static ssm.StartupConstants.CSS_CLASS_LANG_PROMPT;
import static ssm.StartupConstants.STYLE_SHEET_UI;
import ssm.model.Page;

/**
 *
 * @author Randhawa
 */
public class HyperlinkComponent extends Component{
    Page page;
    Stage stage;
    Label headerLabel;
    Scene scn;
    GridPane gridPane;
    TextField headingField;
    Button okButton;
    Button cancelButton;

    public HyperlinkComponent(Page initPage) {
        super(initPage);
        page = initPage;
        stage = new Stage();
        gridPane = new GridPane();
        headerLabel = new Label();
        headerLabel.setText("Enter New Hyperlink");
        headingField = new TextField();
        okButton = new Button("OK");
        cancelButton = new Button("Cancel");
        
        gridPane.add(headerLabel, 0,0,2,1);
        gridPane.add(headingField, 0, 1, 2, 1);
        gridPane.add(okButton, 0, 2, 1,1);
        gridPane.add(cancelButton, 1, 2, 1,1);
        
        Scene scn = new Scene(gridPane);
        headerLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        gridPane.getStyleClass().add(CSS_CLASS_LANG_DIALOG_PANE);
        scn.getStylesheets().add(STYLE_SHEET_UI);
        okButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        cancelButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        stage.setScene(scn);
    }

    public Stage getStage() {
        return stage;
    }
    
    
    
}
