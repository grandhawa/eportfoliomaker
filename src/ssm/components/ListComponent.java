/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssm.components;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import static ssm.StartupConstants.CSS_CLASS_LANG_DIALOG_PANE;
import static ssm.StartupConstants.CSS_CLASS_LANG_OK_BUTTON;
import static ssm.StartupConstants.CSS_CLASS_LANG_PROMPT;
import static ssm.StartupConstants.ICON_ADD_SLIDE;
import static ssm.StartupConstants.ICON_BULLET;
import static ssm.StartupConstants.ICON_REMOVE_SLIDE;
import static ssm.StartupConstants.STYLE_SHEET_UI;
import ssm.model.Page;
import ssm.view.EPortfolioMakerView;

/**
 *
 * @author Randhawa
 */
public class ListComponent extends Component {

    Page page;
    Stage stage;
    Scene scn;
    VBox itemBox;
    GridPane gridPane;
    ScrollPane scroll;
    HBox btnBox;
    Label listLabel;
    Button addButton;
    Button removeButton;
    ObservableList<HBox> items;
    ObservableList<TextField> textFields;
    Button okButton;
    Button cancelButton;
    Label lbl;
    int index = -1;
    Text listText;
    EPortfolioMakerView ui;

    public ListComponent(Page initPage, EPortfolioMakerView initUi) {
        super(initPage);
        ui = initUi;
        page = initPage;
        stage = new Stage();
        listLabel = new Label("Enter List");
        itemBox = new VBox();
        gridPane = new GridPane();
        btnBox = new HBox();
        okButton = new Button("OK");
        cancelButton = new Button("Cancel");
        lbl = new Label();
        lbl.setText("o");
        items = FXCollections.observableArrayList();
        textFields = FXCollections.observableArrayList();

        addButton = new Button("ADD");
        //Image buttonImage = new Image(ICON_ADD_SLIDE);
        //addButton.setGraphic(new ImageView(buttonImage));

        removeButton = new Button("REMOVE");
        //buttonImage = new Image(ICON_REMOVE_SLIDE);
        //removeButton.setGraphic(new ImageView(buttonImage));
//        btnBox.getChildren().add(addButton);
//        btnBox.getChildren().add(removeButton);

        gridPane.add(listLabel, 0, 0, 2, 1);
        gridPane.add(addButton, 0, 1, 2, 1);
        gridPane.add(removeButton, 1, 1, 2, 1);
        gridPane.add(itemBox, 0, 2, 2, 1);
        gridPane.add(okButton, 0, 3, 1, 1);
        gridPane.add(cancelButton, 1, 3, 1, 1);
        scroll = new ScrollPane(gridPane);
        scn = new Scene(scroll);

        lbl.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        listLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        gridPane.getStyleClass().add(CSS_CLASS_LANG_DIALOG_PANE);
        scn.getStylesheets().add(STYLE_SHEET_UI);
        addButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        removeButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        okButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        cancelButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        eventHandlers(stage);
        addItem();
        
        stage.setScene(scn);
        //this.show();
    }

  

    public void startListUi() {

       // itemBox.getChildren().add(btnBox);
        addItem();
        eventHandlers(stage);
        stage.setScene(scn);
        stage.show();
    }

    public void addItem() {
        HBox item = new HBox();
        // Button btn = new Button(" ");
        lbl = new Label("o");
        lbl.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        TextField textField = new TextField();
        item.getChildren().addAll(lbl, textField);
        textFields.add(textField);
        items.add(item);
        reload();
    }

    public void removeItem() {
        items.remove(items.size() - 1);
        reload();
    }

    public void reload() {
        itemBox.getChildren().clear();
        for (HBox hbox : items) {
            itemBox.getChildren().add(hbox);
        }
        if (items.size() == 0) {
            removeButton.setDisable(true);
        } else {
            removeButton.setDisable(false);
        }
    }

    public Stage getStage() {
        return stage;
    }

    @Override
    public void editComponent() {
        stage.showAndWait();
    }

    public void eventHandlers(Stage stage) {
         addButton.setOnAction(e -> {
            addItem();
        });
        removeButton.setOnAction(e -> {
            removeItem();
        });

        okButton.setOnAction(e -> {
            if (page.getComponents().contains(this)) {
                index = page.getComponents().indexOf(this);
                page.getComponents().remove(index);
                this.getChildren().clear();
            }

            //floatSelection = floatComboBox.getValue().toString();
            //String path = selectedFile.getPath();
            String path = "file:/Users/Randhawa/Documents/cse 219/EPortfolioMaker/SlideShowMaker/images/icons/video.jpg";
            System.out.println(path);

            //get index of current page
            //int index = ui.getPageEditorPane().getSelectionModel().getSelectedIndex();
            for (TextField textField : textFields) {
                     HBox listItem = new HBox();
                     //listItem.getChildren().add(lbl);
                     listText = new Text(textField.getText());
                     listItem.getChildren().add(listText);
                     this.getChildren().add(listItem);
            }
            //listText.setText(headingField.getText());

            //put the image in the vBox of that box
            //this.getChildren().add(paragraph);
            //if the value of index has been changed
            if (index != -1) {
                page.getComponents().add(index, this);
            } else {
                page.getComponents().add(this);
            }

            ui.reloadTabsPane();
            stage.hide();
        });
        cancelButton.setOnAction(e -> {
            stage.hide();
        });
    }

}

 
