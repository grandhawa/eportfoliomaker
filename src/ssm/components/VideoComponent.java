/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssm.components;

import java.io.File;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.json.Json;
import javax.json.JsonObject;
import static ssm.StartupConstants.CSS_CLASS_LANG_COMBO_BOX;
import static ssm.StartupConstants.CSS_CLASS_LANG_DIALOG_PANE;
import static ssm.StartupConstants.CSS_CLASS_LANG_OK_BUTTON;
import static ssm.StartupConstants.CSS_CLASS_LANG_PROMPT;
import static ssm.StartupConstants.ICON_FONT;
import static ssm.StartupConstants.STYLE_SHEET_UI;
import static ssm.file.EPortfolioFileManager.JSON_CAPTION;
import static ssm.file.EPortfolioFileManager.JSON_IMAGE_FILE_NAME;
import static ssm.file.EPortfolioFileManager.JSON_IMAGE_PATH;
import ssm.model.Page;
import ssm.view.EPortfolioMakerView;

/**
 *
 * @author Randhawa
 */
public class VideoComponent extends Component {
    Page page;
    Stage stage;
    Label headerLabel;
    Scene scn;
    GridPane gridPane;
    TextField heightField;
    TextField widthField;
    TextField captionField;
    FileChooser fileChooser;
    Label heightLabel;
    Label widthLabel;
    Label captionLabel;
    //Label floatLabel;
    //ComboBox floatComboBox;
    Button browseButton;
    Button okButton;
    Button cancelButton;
    File selectedFile;
    Label fileLabel;
    int index = -1;
    EPortfolioMakerView ui;
    String imagePath = "file:/Users/Randhawa/Documents/cse 219/EPortfolioMaker/SlideShowMaker/images/icons/video.jpg";
    
    public VideoComponent(Page initPage, EPortfolioMakerView initUi){
        super(initPage);
        ui = initUi;
        page = initPage;
        //initialize everything
        stage = new Stage();
        gridPane = new GridPane();
        stage.setTitle("Video component dialog");
        
        //intializing labels and text fields
        fileLabel = new Label();
        headerLabel = new Label();
        headerLabel.setText("New Video Properties");
        captionLabel = new Label();
        captionLabel.setText("Caption: ");
        //floatLabel = new Label();
        //floatLabel.setText("Video Float");
        heightLabel = new Label();
        heightLabel.setText("Height: ");
        heightField = new TextField();
        widthLabel = new Label();
        widthLabel.setText("Width: ");
        widthField = new TextField();
        captionField = new TextField();
        fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter("Video Files", "*.mp4", "*.avi", "*.m4v"));
        browseButton = new Button();
        browseButton.setText("Browse Video");
        okButton = new Button("OK");
        cancelButton = new Button("Cancel");
        
        //set up combo box
//        ObservableList<String> floatChoices = FXCollections.observableArrayList();
//	floatChoices.add("Left");
//	floatChoices.add("Right");
//        floatChoices.add("Neither");
//	floatComboBox = new ComboBox(floatChoices);
//	floatComboBox.getSelectionModel().select("Left");
        
        //add everything into the grid pane
        gridPane.add(headerLabel, 0,0,2,1);
        
        gridPane.add(captionLabel, 0, 1, 1, 1);
        gridPane.add(captionField, 1,1,1,1);
        
        gridPane.add(heightLabel, 0, 2, 1, 1);
        gridPane.add(heightField, 1,2,1,1);
        
        gridPane.add(widthLabel, 0, 3, 1, 1);
        gridPane.add(widthField, 1,3,1,1);
        
        //gridPane.add(floatLabel, 0, 4, 1, 1);
        //gridPane.add(floatComboBox, 1,4,1,1);
        
        gridPane.add(browseButton,0,4,1,1);
        gridPane.add(fileLabel, 1, 4, 1, 1);
        gridPane.add(okButton, 0, 5, 1,1);
        gridPane.add(cancelButton, 1, 5, 1,1);
        
        //set scene
        Scene scn = new Scene(gridPane);
        headerLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        gridPane.getStyleClass().add(CSS_CLASS_LANG_DIALOG_PANE);
        //floatComboBox.getStyleClass().add(CSS_CLASS_LANG_COMBO_BOX);
        scn.getStylesheets().add(STYLE_SHEET_UI);
        browseButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        okButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        cancelButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        stage.setScene(scn);
        eventHandlers(stage);
    } 

    public Stage getStage() {
        return stage;
    }
    
     @Override
    public void editComponent() {
        stage.showAndWait();
    }
    
    @Override
     public JsonObject makeComponentJsonObject() {
	JsonObject jso = Json.createObjectBuilder()
                .add("component_type", "Video")
		.add("video_path", selectedFile.getPath())
		.add("video_caption", captionField.getText())
                .add("video_height", heightField.getText())
                .add("video_width", widthField.getText())
		.build();
	return jso;
    }
     
      public void loadComponentJsonObject(JsonObject jso) {
        int videoHeight = Integer.parseInt(jso.getString("video_height"));
        int videoWidth = Integer.parseInt(jso.getString("video_width"));
        String path = jso.getString("video_path");
        String caption = jso.getString("video_caption");
        setVideoComponent(videoHeight, videoWidth, path,
            caption);
        page.getComponents().add(this);
	//ui.reloadTabsPane();
    }
    
    public void eventHandlers(Stage stage) {
        browseButton.setOnAction(e -> {
            selectedFile = fileChooser.showOpenDialog(stage);
            if (selectedFile != null) {
                fileLabel.setText(selectedFile.getName());
                System.out.println(selectedFile.getName());
            } else {
                System.out.println("File is null");
            }
        });
        
       

        okButton.setOnAction(e -> {
            if (page.getComponents().contains(this)) {
                index = page.getComponents().indexOf(this);
                page.getComponents().remove(index);
                this.getChildren().clear();
            }
            int videoHeight = Integer.parseInt(heightField.getText());
            int videoWidth = Integer.parseInt(widthField.getText());
            //floatSelection = floatComboBox.getValue().toString();
            String path = selectedFile.getPath();
            String caption = captionField.getText();
            setVideoComponent(videoHeight,videoWidth,path,caption);
//            System.out.println(imagePath);
//            Image img = new Image(imagePath);
//            ImageView imgView = new ImageView(img);
//            imgView.setFitHeight(videoHeight);
//            imgView.setFitWidth(videoWidth);
//            //get index of current page
//            //int index = ui.getPageEditorPane().getSelectionModel().getSelectedIndex();
//
//            //put the image in the vBox of that box
//            this.getChildren().add(imgView);
            //if the value of index has been changed
            if (index!=-1) {
                page.getComponents().add(index, this);
            } else {
                page.getComponents().add(this);
            }

            ui.reloadTabsPane();
            stage.hide();
        });
        cancelButton.setOnAction(e ->{
            stage.hide();
        });
    }
    public void setVideoComponent(int initImageHeight, int initImageWidth,  String initPath,
            String initCaption){
        
            Image img = new Image(imagePath);
            ImageView imgView = new ImageView(img);
            imgView.setFitHeight(initImageHeight);
            imgView.setFitWidth(initImageWidth);
            Label pathLbl = new Label("Paht of video: "+initPath);
            Label captionLbl = new Label("Caption: "+initCaption);
            this.getChildren().addAll(imgView,captionLbl,pathLbl);
    }

    
}
