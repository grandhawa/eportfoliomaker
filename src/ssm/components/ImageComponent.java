/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssm.components;

import java.io.File;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javax.json.Json;
import javax.json.JsonObject;
import static ssm.StartupConstants.CSS_CLASS_LANG_COMBO_BOX;
import static ssm.StartupConstants.CSS_CLASS_LANG_DIALOG_PANE;
import static ssm.StartupConstants.CSS_CLASS_LANG_OK_BUTTON;
import static ssm.StartupConstants.CSS_CLASS_LANG_PROMPT;
import static ssm.StartupConstants.STYLE_SHEET_UI;
import static ssm.file.EPortfolioFileManager.JSON_CAPTION;
import static ssm.file.EPortfolioFileManager.JSON_IMAGE_FILE_NAME;
import static ssm.file.EPortfolioFileManager.JSON_IMAGE_PATH;
import ssm.model.EPortfolioModel;
import ssm.model.Page;
import ssm.view.EPortfolioMakerView;

/**
 *
 * @author Randhawa
 */
public class ImageComponent extends Component {

    Stage stage;
    Page page;
    EPortfolioMakerView ui;
    //EPortfolioModel ePortfolio;
    Label headerLabel;
    Scene scn;
    GridPane gridPane;
    TextField heightField;
    TextField widthField;
    TextField captionField;
    FileChooser fileChooser;
    Label heightLabel;
    Label widthLabel;
    Label captionLabel;
    Label floatLabel;
    Label fileLabel;
    ComboBox floatComboBox;
    Button browseButton;
    Button okButton;
    Button cancelButton;

    File selectedFile;
    int index = -1;

    public ImageComponent(EPortfolioMakerView initUi, Page initPage) {
        super(initPage);
        //initialize everything
        page = initPage;
        stage = new Stage();
        ui = initUi;
        //ePortfolio = ePortfolioModel;
        gridPane = new GridPane();
        stage.setTitle("Image component dialog");

        //intializing labels and text fields
        headerLabel = new Label();
        headerLabel.setText("New Image Properties");
        captionLabel = new Label();
        captionLabel.setText("Caption: ");
        floatLabel = new Label();
        floatLabel.setText("Image Float");
        heightLabel = new Label();
        heightLabel.setText("Height: ");
        heightField = new TextField();
        widthLabel = new Label();
        widthLabel.setText("Width: ");
        fileLabel = new Label();
        widthField = new TextField();
        captionField = new TextField();
        browseButton = new Button();
        browseButton.setText("Browse Image");
        okButton = new Button("OK");
        cancelButton = new Button("Cancel");

        //setup file chooser
        fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(
                new ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));

        //set up combo box
        ObservableList<String> floatChoices = FXCollections.observableArrayList();
        floatChoices.add("Left");
        floatChoices.add("Right");
        floatChoices.add("Neither");
        floatComboBox = new ComboBox(floatChoices);
        floatComboBox.getSelectionModel().select("Left");

        //add everything into the grid pane
        gridPane.add(headerLabel, 0, 0, 2, 1);

        gridPane.add(captionLabel, 0, 1, 1, 1);
        gridPane.add(captionField, 1, 1, 1, 1);

        gridPane.add(heightLabel, 0, 2, 1, 1);
        gridPane.add(heightField, 1, 2, 1, 1);

        gridPane.add(widthLabel, 0, 3, 1, 1);
        gridPane.add(widthField, 1, 3, 1, 1);

        gridPane.add(floatLabel, 0, 4, 1, 1);
        gridPane.add(floatComboBox, 1, 4, 1, 1);

        gridPane.add(browseButton, 0, 5, 1, 1);
        gridPane.add(fileLabel, 1, 5, 1, 1);
        gridPane.add(okButton, 0, 6, 1, 1);
        gridPane.add(cancelButton, 1, 6, 1, 1);

        //set scene
        scn = new Scene(gridPane);
        headerLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        gridPane.getStyleClass().add(CSS_CLASS_LANG_DIALOG_PANE);
        floatComboBox.getStyleClass().add(CSS_CLASS_LANG_COMBO_BOX);
        scn.getStylesheets().add(STYLE_SHEET_UI);
        browseButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        okButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        cancelButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        stage.setScene(scn);
        eventHandlers(stage);
    }

    public Stage getStage() {
        return stage;
    }

    @Override
    public void editComponent() {
        stage.showAndWait();
    }

    /**
     *
     * @return
     */
    @Override
     public JsonObject makeComponentJsonObject() {
	JsonObject jso = Json.createObjectBuilder()
                .add("component_type", "Image")
		.add("image_path", selectedFile.getPath())
		.add("image_caption", captionField.getText())
                .add("image_height", heightField.getText())
                .add("image_width", widthField.getText())
                .add("float_selection", floatComboBox.getValue().toString())
		.build();
	return jso;
    }
     
      public void loadComponentJsonObject(JsonObject jso) {
        int imageHeight = Integer.parseInt(jso.getString("image_height"));
        int imageWidth = Integer.parseInt(jso.getString("image_width"));
        String path = jso.getString("image_path");
        String caption = jso.getString("image_caption");
        setImageComponent(imageHeight, imageWidth, path,
            caption);
        page.getComponents().add(this);
	//ui.reloadTabsPane();
    }

    public void eventHandlers(Stage stage) {
        browseButton.setOnAction(e -> {
            selectedFile = fileChooser.showOpenDialog(stage);
            if (selectedFile != null) {
                fileLabel.setText(selectedFile.getName());
                System.out.println(selectedFile.getName());
            } else {
                System.out.println("File is null");
            }
        });

        okButton.setOnAction(e -> {
            if (page.getComponents().contains(this)) {
                index = page.getComponents().indexOf(this);
                page.getComponents().remove(index);
                this.getChildren().clear();
            }
            int imageHeight = Integer.parseInt(heightField.getText());
            int imageWidth = Integer.parseInt(widthField.getText());
            String floatSelection = floatComboBox.getValue().toString();
            String path = selectedFile.getPath();
            String caption = captionField.getText();
            setImageComponent(imageHeight, imageWidth, path,caption);
            //if the value of index has been changed
            if (index != -1) {
                page.getComponents().add(index, this);
            } else {
                page.getComponents().add(this);
            }

            ui.reloadTabsPane();
            stage.hide();
        });
        cancelButton.setOnAction(e -> {
            stage.hide();
        });
    }
    
    public void setImageComponent(int initImageHeight, int initImageWidth,  String initpath,
            String initCaption){
        
            initpath = "file:" + initpath;
            System.out.println(initpath);
            Image img = new Image(initpath);
            ImageView imgView = new ImageView(img);
            imgView.setFitHeight(initImageHeight);
            imgView.setFitWidth(initImageWidth);
            Label captionLbl = new Label("Caption: "+initCaption);
            this.getChildren().addAll(imgView,captionLbl);
    }

}
