package ssm.view;

//import java.nio.file.Paths;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Path;
import javafx.scene.web.WebView;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.LanguagePropertyType.LABEL_SLIDESHOW_TITLE;
import static ssm.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_PANE;
import static ssm.StartupConstants.CSS_CLASS_VERTICAL_TOOLBAR_PANE;
import static ssm.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON;
import static ssm.StartupConstants.CSS_CLASS_LAYOUT_TOOLBAR_BUTTON;
import static ssm.StartupConstants.CSS_CLASS_LAYOUT_TOOLBAR_PANE;
import static ssm.StartupConstants.CSS_CLASS_SLIDES_EDITOR_PANE;
import static ssm.StartupConstants.CSS_CLASS_TITLE_PANE;
import static ssm.StartupConstants.CSS_CLASS_TITLE_PROMPT;
import static ssm.StartupConstants.CSS_CLASS_TITLE_TEXT_FIELD;
import static ssm.StartupConstants.CSS_CLASS_VERTICAL_TOOLBAR_BUTTON;
import static ssm.StartupConstants.CSS_CLASS_WORKSPACE;
import static ssm.StartupConstants.ICON_ADD_SLIDE;
import static ssm.StartupConstants.ICON_BANNER_IMAGE;
import static ssm.StartupConstants.ICON_COLOR;
import static ssm.StartupConstants.ICON_EXIT;
import static ssm.StartupConstants.ICON_EXPORT;
import static ssm.StartupConstants.ICON_FONT;
import static ssm.StartupConstants.ICON_FOOTER;
import static ssm.StartupConstants.ICON_HEADING;
import static ssm.StartupConstants.ICON_HYPERLINK;
import static ssm.StartupConstants.ICON_IMAGE;
import static ssm.StartupConstants.ICON_LAYOUT;
import static ssm.StartupConstants.ICON_LIST;
import static ssm.StartupConstants.ICON_LOAD_SLIDE_SHOW;
import static ssm.StartupConstants.ICON_NEW_SLIDE_SHOW;
import static ssm.StartupConstants.ICON_PARAGRAPH;
import static ssm.StartupConstants.ICON_REMOVE_COMP;
import static ssm.StartupConstants.ICON_REMOVE_SLIDE;
import static ssm.StartupConstants.ICON_SAVE_SLIDE_SHOW;
import static ssm.StartupConstants.ICON_SLIDESHOW;
import static ssm.StartupConstants.ICON_STUDENTNAME;
import static ssm.StartupConstants.ICON_SWITCH;
import static ssm.StartupConstants.ICON_VIDEO;
import static ssm.StartupConstants.PATH_ICONS;
import static ssm.StartupConstants.STYLE_SHEET_UI;
import ssm.components.BannerImageComponent;
import ssm.components.Component;
import ssm.components.FooterComponent;
import ssm.components.HeadingComponent;
import ssm.components.HyperlinkComponent;
import ssm.components.ImageComponent;
import ssm.components.ListComponent;
import ssm.components.ParagraphComponent;
import ssm.components.SlideShowComponent;
import ssm.components.StudentNameComponent;
import ssm.components.VideoComponent;
import ssm.controller.FileController;
import ssm.controller.SlideShowEditController;
import ssm.model.SlideShowModel;
import ssm.error.ErrorHandler;
import ssm.file.EPortfolioFileManager;
import ssm.file.SlideShowFileManager;
import ssm.file.SlideShowSiteExporter;
import ssm.model.EPortfolioModel;
import ssm.model.Page;

/**
 * This class provides the User Interface for this application, providing
 * controls and the entry points for creating, loading, saving, editing, and
 * viewing slide shows.
 *
 * @author McKilla Gorilla & _____________
 */
public class EPortfolioMakerView {

    // THIS IS THE MAIN APPLICATION UI WINDOW AND ITS SCENE GRAPH
    Stage primaryStage;
    Scene primaryScene;

    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    BorderPane ssmPane;

    // THIS IS THE TOP TOOLBAR AND ITS CONTROLS
    FlowPane fileToolbarPane;
    Button newEPortfolioButton;
    Button loadEPortfolioButton;
    Button saveEPortfolioButton;
    Button viewEPortfolioButton;
    Button exportButton;
    Button exitButton;

    // WORKSPACE
    BorderPane workspace;

    // THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
    FlowPane slideEditToolbar;
    Button addPageButton;
    Button removePageButton;
    Button removeComponent;

    // THIS WILL GO IN THE RIGHT SIDE OF THE SCREEN
    HBox rightPanes;
    Button colorButton;
    Button layoutButton;
    Button fontButton;

    //First Pane
    FlowPane layoutComponentToolbar;
    //Second Pane
    FlowPane componentToolbar;
    Button imageButton;
    Button slideShowButton;
    Button videoButton;
    Button bannerImageButton;
    Button switchButton;

    //Third Pane
    FlowPane textComponentToolbar;
    Button headingButton;
    Button paragraphButton;
    Button listButton;
    Button hlButton;
    Button footerButton;
    Button studentName;

    // FOR THE SLIDE SHOW TITLE
    FlowPane titlePane;
    Label titleLabel;
    TextField titleTextField;

    // AND THIS WILL GO IN THE CENTER
    //ScrollPane slidesEditorScrollPane;
    TabPane pageEditorPane;

    WebView web = new WebView();

    // THIS IS THE SLIDE SHOW WE'RE WORKING WITH
    SlideShowModel slideShow;

    //Eportfolio model
    EPortfolioModel ePortfolio;

    // THIS IS FOR SAVING AND LOADING EPORTFOLIOS
    EPortfolioFileManager fileManager;

    // THIS IS FOR SAVING AND LOADING SLIDE SHOWS
    SlideShowFileManager slideShowfileManager;

    // THIS IS FOR EXPORTING THE SLIDESHOW SITE
    SlideShowSiteExporter siteExporter;

    // THIS CLASS WILL HANDLE ALL ERRORS FOR THIS PROGRAM
    private ErrorHandler errorHandler;

    // THIS CONTROLLER WILL ROUTE THE PROPER RESPONSES
    // ASSOCIATED WITH THE FILE TOOLBAR
    private FileController fileController;

    // THIS CONTROLLER RESPONDS TO SLIDE SHOW EDIT BUTTONS
    private SlideShowEditController editController;

    ImageComponent imgComponent;
    SlideShowComponent slideComponent;
    VideoComponent videoComponent;
    HeadingComponent headingComponent;
    ParagraphComponent paragraphComponent;
    ListComponent listComponent;
    HyperlinkComponent hlComponent;
    BannerImageComponent bannerImageComponent;
    StudentNameComponent studentNameComponent;
    FooterComponent footerComponent;

    Boolean switchWorkspace = true;

    /**
     * Default constructor, it initializes the GUI for use, but does not yet
     * load all the language-dependent controls, that needs to be done via the
     * startUI method after the user has selected a language.
     *
     * @param initFileManager
     * @param initSiteExporter
     */
    public EPortfolioMakerView(EPortfolioFileManager initFileManager,
            SlideShowSiteExporter initSiteExporter) {
        ePortfolio = new EPortfolioModel(this);

        // FIRST HOLD ONTO THE FILE MANAGER
        fileManager = initFileManager;
//Path current = Paths.get("");
        slideShowfileManager = new SlideShowFileManager();

        // AND THE SITE EXPORTER
        siteExporter = initSiteExporter;
        //URI url = java.nio.file.Paths.get("/public_html/Page1.html").toAbsolutePath().toUri();
        web.getEngine().load("https://www.google.com");

        // MAKE THE DATA MANAGING MODEL
        //slideShow = new SlideShowModel(this);
        // WE'LL USE THIS ERROR HANDLER WHEN SOMETHING GOES WRONG
        //errorHandler = new ErrorHandler(this);  
    }

    // ACCESSOR METHODS
    public SlideShowModel getSlideShow() {
        return slideShow;
    }

    public EPortfolioModel getEPortfolio() {
        return ePortfolio;
    }

    public Stage getWindow() {
        return primaryStage;
    }

    public ErrorHandler getErrorHandler() {
        return errorHandler;
    }

    public TabPane getPageEditorPane() {
        return pageEditorPane;
    }

    /**
     * Initializes the UI controls and gets it rolling.
     *
     * @param initPrimaryStage The window for this application.
     *
     * @param windowTitle The title for this window.
     */
    public void startUI(Stage initPrimaryStage, String windowTitle) {
        // THE TOOLBAR ALONG THE NORTH
        initFileToolbar();

        // INIT THE CENTER WORKSPACE CONTROLS BUT DON'T ADD THEM
        // TO THE WINDOW YET
        initWorkspace();

        // NOW SETUP THE EVENT HANDLERS
        initEventHandlers();

        // AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
        // KEEP THE WINDOW FOR LATER
        primaryStage = initPrimaryStage;
        initWindow(windowTitle);
    }

    // UI SETUP HELPER METHODS
    private void initWorkspace() {
        // FIRST THE WORKSPACE ITSELF, WHICH WILL CONTAIN TWO REGIONS
        workspace = new BorderPane();

        // THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
        slideEditToolbar = new FlowPane();
        addPageButton = this.initChildButton(slideEditToolbar, ICON_ADD_SLIDE, "Add Page", CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, false);
        removePageButton = this.initChildButton(slideEditToolbar, ICON_REMOVE_SLIDE, "Remove Page", CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, false);
        removeComponent = this.initChildButton(slideEditToolbar, ICON_REMOVE_COMP, "Remove Component", CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, false);
        // THIS WILL GO IN THE RIGHT SIDE OF THE SCREEN
        rightPanes = new HBox();
        //First Toolbar on the right
        layoutComponentToolbar = new FlowPane();
        layoutComponentToolbar.setPrefWidth(200);
        colorButton = this.initChildButton(layoutComponentToolbar, ICON_COLOR, "Add Color", CSS_CLASS_LAYOUT_TOOLBAR_BUTTON, false);
        layoutButton = this.initChildButton(layoutComponentToolbar, ICON_LAYOUT, "Change Layout", CSS_CLASS_LAYOUT_TOOLBAR_BUTTON, false);
        fontButton = this.initChildButton(layoutComponentToolbar, ICON_FONT, "Select font", CSS_CLASS_LAYOUT_TOOLBAR_BUTTON, false);
        //Second Toolbar on the right
        componentToolbar = new FlowPane();
        imageButton = this.initChildButton(componentToolbar, ICON_IMAGE, "Add Image", CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, false);
        slideShowButton = this.initChildButton(componentToolbar, ICON_SLIDESHOW, "Add Slide Show", CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, false);
        videoButton = this.initChildButton(componentToolbar, ICON_VIDEO, "Add Video", CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, false);
        bannerImageButton = this.initChildButton(componentToolbar, ICON_BANNER_IMAGE, "Add Banner Image", CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, false);
        //Third Toolbar on the right
        textComponentToolbar = new FlowPane();
        headingButton = this.initChildButton(textComponentToolbar, ICON_HEADING, "Add Heading", CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, false);
        paragraphButton = this.initChildButton(textComponentToolbar, ICON_PARAGRAPH, "Add Paragraph", CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, false);
        listButton = this.initChildButton(textComponentToolbar, ICON_LIST, "Add List", CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, false);
        hlButton = this.initChildButton(textComponentToolbar, ICON_HYPERLINK, "Add Hyperlink", CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, false);
        studentName = this.initChildButton(textComponentToolbar, ICON_STUDENTNAME, "Student Name", CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, false);
        footerButton = this.initChildButton(textComponentToolbar, ICON_FOOTER, "Add Footer", CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, false);
        // AND THIS WILL GO IN THE CENTER
        pageEditorPane = new TabPane();
        initTitleControls();

        // NOW PUT THESE IN THE WORKSPACE
        workspace.setLeft(slideEditToolbar);
        workspace.setCenter(pageEditorPane);

        rightPanes.getChildren().addAll(layoutComponentToolbar, componentToolbar, textComponentToolbar);
        workspace.setRight(rightPanes);

        // SETUP ALL THE STYLE CLASSES
        workspace.getStyleClass().add(CSS_CLASS_WORKSPACE);
        slideEditToolbar.getStyleClass().add(CSS_CLASS_VERTICAL_TOOLBAR_PANE);
        componentToolbar.getStyleClass().add(CSS_CLASS_VERTICAL_TOOLBAR_PANE);
        textComponentToolbar.getStyleClass().add(CSS_CLASS_VERTICAL_TOOLBAR_PANE);
        layoutComponentToolbar.getStyleClass().add(CSS_CLASS_LAYOUT_TOOLBAR_PANE);
//	pageEditorPane.getStyleClass().add(CSS_CLASS_SLIDES_EDITOR_PANE);
        //slidesEditorScrollPane.getStyleClass().add(CSS_CLASS_SLIDES_EDITOR_PANE);
    }

    private void initEventHandlers() {
        // FIRST THE FILE CONTROLS
        fileController = new FileController(this, fileManager, siteExporter);
        newEPortfolioButton.setOnAction(e -> {
            fileController.handleNewEPortfolioRequest();
        });

        loadEPortfolioButton.setOnAction(e -> {
            fileController.handleLoadEPortfolioRequest();
        });

        saveEPortfolioButton.setOnAction(e -> {
            fileController.handleSaveEPortfolioRequest();
        });

        switchButton.setOnAction(e -> {
            if (switchWorkspace == true) {
                workspace.setVisible(false);
                web.setVisible(true);
                ssmPane.setCenter(web);
            } else {
                workspace.setVisible(true);
                web.setVisible(false);
                ssmPane.setCenter(workspace);
            }
            switchWorkspace = !switchWorkspace;
            //fileController.handleViewEPortfolioRequest();
        });

        exitButton.setOnAction(e -> {
            fileController.handleExitRequest();
        });

        // THEN THE SLIDE SHOW EDIT CONTROLS
        editController = new SlideShowEditController(this);
        addPageButton.setOnAction(e -> {

            ePortfolio.addTab();
        });
        removePageButton.setOnAction(e -> {
            int currIndex = pageEditorPane.getSelectionModel().getSelectedIndex();
            Tab tabToRemove = pageEditorPane.getSelectionModel().getSelectedItem();
            // pageEditorPane.getTabs().remove(tabToRemove);
            ePortfolio.removeTab(tabToRemove, currIndex);
        });

        removeComponent.setOnAction(e -> {
            getCurrPage().removeSelectedComponent();
        });

        //FOR THE COMPONENT BUTTONS ON RIGHT
        imageButton.setOnAction(e -> {
            imgComponent = new ImageComponent(this, getCurrPage());
            imgComponent.getStage().showAndWait();
        });

        slideShowButton.setOnAction(e -> {
            slideComponent = new SlideShowComponent(slideShowfileManager, siteExporter, getCurrPage(), this);
            slideComponent.startUI("Slide Show Editor");
        });

        videoButton.setOnAction(e -> {
            videoComponent = new VideoComponent(getCurrPage(), this);
            videoComponent.getStage().showAndWait();
        });

        headingButton.setOnAction(e -> {
            headingComponent = new HeadingComponent(getCurrPage(), this);
            headingComponent.getStage().showAndWait();
        });

        paragraphButton.setOnAction(e -> {
            paragraphComponent = new ParagraphComponent(getCurrPage(), this);
            paragraphComponent.getStage().showAndWait();
        });

        listButton.setOnAction(e -> {
            listComponent = new ListComponent(getCurrPage(), this);
            listComponent.getStage().showAndWait();
        });

        hlButton.setOnAction(e -> {
            hlComponent = new HyperlinkComponent(getCurrPage());
            hlComponent.getStage().showAndWait();
        });
        bannerImageButton.setOnAction(e -> {
            bannerImageComponent = new BannerImageComponent(this, getCurrPage());
            bannerImageComponent.getStage().showAndWait();
        });
        studentName.setOnAction(e -> {
            studentNameComponent = new StudentNameComponent(this, getCurrPage());
            studentNameComponent.getStage().showAndWait();
        });
        footerButton.setOnAction(e -> {
            footerComponent = new FooterComponent(this, getCurrPage());
            footerComponent.getStage().showAndWait();
        });

        layoutButton.setOnAction(e -> {
            List<String> choices = new ArrayList<>();
            choices.add("Layout 1");
            choices.add("Layout 2");
            choices.add("Layout 3");

            ChoiceDialog<String> dialog = new ChoiceDialog<>("Layout 1", choices);
            dialog.setTitle("Layout Dialog");
            dialog.setHeaderText("Layout");
            dialog.setContentText("Choose your layout:");

// Traditional way to get the response value.
            Optional<String> result = dialog.showAndWait();
            if (result.isPresent()) {
                System.out.println("Your choice: " + result.get());
            }
        });
    }

    public Page getCurrPage() {
        int index = pageEditorPane.getSelectionModel().getSelectedIndex();
        Page page = ePortfolio.getPages().get(index);
        return page;
    }

    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    private void initFileToolbar() {
        fileToolbarPane = new FlowPane();
        fileToolbarPane.getStyleClass().add(CSS_CLASS_HORIZONTAL_TOOLBAR_PANE);

        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        newEPortfolioButton = initChildButton(fileToolbarPane, ICON_NEW_SLIDE_SHOW, "New E-Portfolio", CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        loadEPortfolioButton = initChildButton(fileToolbarPane, ICON_LOAD_SLIDE_SHOW, "Load E-Portflio", CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        saveEPortfolioButton = initChildButton(fileToolbarPane, ICON_SAVE_SLIDE_SHOW, "Save", CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        exportButton = initChildButton(fileToolbarPane, ICON_EXPORT, "Export", CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        exitButton = initChildButton(fileToolbarPane, ICON_EXIT, "Exit", CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        switchButton = initChildButton(fileToolbarPane, ICON_SWITCH, "Switch Mode", CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        //switchButton.setAlignment(Pos.TOP_RIGHT);
    }

    private void initWindow(String windowTitle) {
        // SET THE WINDOW TITLE
        primaryStage.setTitle(windowTitle);

        // GET THE SIZE OF THE SCREEN
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        // AND USE IT TO SIZE THE WINDOW
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());

        // SETUP THE UI, NOTE WE'LL ADD THE WORKSPACE LATER
        ssmPane = new BorderPane();
        ssmPane.getStyleClass().add(CSS_CLASS_WORKSPACE);
        ssmPane.setTop(fileToolbarPane);
        primaryScene = new Scene(ssmPane);

        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
        // WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
        primaryScene.getStylesheets().add(STYLE_SHEET_UI);
        primaryStage.setScene(primaryScene);
        primaryStage.show();
    }

    /**
     * This helps initialize buttons in a toolbar, constructing a custom button
     * with a customly provided icon and tooltip, adding it to the provided
     * toolbar pane, and then returning it.
     */
    public Button initChildButton(
            Pane toolbar,
            String iconFileName,
            String tooltip,
            String cssClass,
            boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_ICONS + iconFileName;
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.getStyleClass().add(cssClass);
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(tooltip);
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }

    /**
     * Updates the enabled/disabled status of all toolbar buttons.
     *
     * @param saved
     */
    public void updateFileToolbarControls(boolean saved) {
        // FIRST MAKE SURE THE WORKSPACE IS THERE
        ssmPane.setCenter(workspace);

        // NEXT ENABLE/DISABLE BUTTONS AS NEEDED IN THE FILE TOOLBAR
        //saveEPortfolioButton.setDisable(saved);
        //viewEPortfolioButton.setDisable(false);
        //updateSlideshowEditToolbarControls();
    }

    public void updateSlideshowEditToolbarControls() {
        // AND THE SLIDESHOW EDIT TOOLBAR
        addPageButton.setDisable(false);
        boolean slideSelected = slideShow.isSlideSelected();
        //removeSlideButton.setDisable(!slideSelected);
//	moveSlideUpButton.setDisable(!slideSelected);
//	moveSlideDownButton.setDisable(!slideSelected);	
    }

    /**
     * Uses the slide show data to reload all the components for slide editing.
     *
     * @param slideShowToLoad SLide show being reloaded.
     */
//    public void reloadSlideShowPane() {
//	pageEditorPane.getTabs().clear();
//	reloadTitleControls();
//	for (Slide slide : slideShow.getSlides()) {
//	    SlideEditView slideEditor = new SlideEditView(this, slide);
//	    if (slideShow.isSelectedSlide(slide))
//		slideEditor.getStyleClass().add(CSS_CLASS_SELECTED_SLIDE_EDIT_VIEW);
//	    else
//		slideEditor.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW);
//	    pageEditorPane.getTabs().add(slideEditor);
//	    slideEditor.setOnMousePressed(e -> {
//		slideShow.setSelectedSlide(slide);
//		this.reloadSlideShowPane();
//	    });
//	}
//	updateSlideshowEditToolbarControls();
//    }
    public void reloadTabsPane() {
        pageEditorPane.getTabs().clear();
        int i = 0;

        for (Tab tab : ePortfolio.getTabs()) {
            ScrollPane sPane;
            sPane = new ScrollPane();
            sPane.getStyleClass().add(CSS_CLASS_SLIDES_EDITOR_PANE);
            VBox vbox = new VBox();
            Page page = ePortfolio.getPages().get(i);
            vbox.getChildren().add(page.getPageTitleBox());
            if (page.getStudentName() != null) {
                vbox.getChildren().add(page.getStudentName());
            }
            if (page.getBannerImage() != null) {
                vbox.getChildren().add(page.getBannerImage());
            }
            for (Component comp : page.getComponents()) {
                vbox.getChildren().add(comp);
            }

            if (page.getFooter() != null) {
                vbox.getChildren().add(page.getFooter());
            }
            sPane.setContent(vbox);
            tab.setContent(sPane);
            pageEditorPane.getTabs().add(tab);
            i++;
        }
        if (ePortfolio.getTabs().size() == 0) {
            removePageButton.setDisable(true);
        } else {
            removePageButton.setDisable(false);
        }
    }

    private void initTitleControls() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String labelPrompt = props.getProperty(LABEL_SLIDESHOW_TITLE);
        titlePane = new FlowPane();
        titleLabel = new Label(labelPrompt);
        titleTextField = new TextField();

        titlePane.getChildren().add(titleLabel);
        titlePane.getChildren().add(titleTextField);

        String titlePrompt = props.getProperty(LanguagePropertyType.LABEL_SLIDESHOW_TITLE);
        titleTextField.setText(titlePrompt);

        titleTextField.textProperty().addListener(e -> {
            slideShow.setTitle(titleTextField.getText());
            updateFileToolbarControls(false);
        });

        titlePane.getStyleClass().add(CSS_CLASS_TITLE_PANE);
        titleLabel.getStyleClass().add(CSS_CLASS_TITLE_PROMPT);
        titleTextField.getStyleClass().add(CSS_CLASS_TITLE_TEXT_FIELD);
    }
//    
//    public void reloadTitleControls() {
//	if (pageEditorPane.getTabs().size() == 0)
//	    pageEditorPane.getTabs().add(titlePane);
//	titleTextField.setText(slideShow.getTitle());
//    }
}
