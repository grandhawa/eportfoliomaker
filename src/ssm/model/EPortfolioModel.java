/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssm.model;

import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Tab;
import javafx.scene.control.TextInputDialog;
import ssm.view.EPortfolioMakerView;

/**
 *
 * @author Randhawa
 */
public class EPortfolioModel {

    EPortfolioMakerView ui;
    ObservableList<Tab> tabs;
    ObservableList<Page> pages;

    public EPortfolioModel(EPortfolioMakerView ui) {
        this.ui = ui;
        tabs = FXCollections.observableArrayList();
        pages = FXCollections.observableArrayList();
    }

    public void addTab() {
        TextInputDialog dialog = new TextInputDialog("New Page");
        dialog.setTitle("New Page Name");
        dialog.setHeaderText("New Page !");
        dialog.setContentText("Enter a name for new page:");

        // get the response value.
        Optional<String> result = dialog.showAndWait();
        String newPageName = "New Page";
        if (result.isPresent()) {
            newPageName = result.get();

            Tab newTab = new Tab(newPageName);
            tabs.add(newTab);
            Page page = new Page(newTab, ui);
            pages.add(page);
            ui.reloadTabsPane();
        }
    }

    /**
     * 
     * @param title 
     */
    public void addTab(String title) {
        String newPageName = title;
        Tab newTab = new Tab(newPageName);
        tabs.add(newTab);
        Page page = new Page(newTab, ui);
        pages.add(page);
        ui.reloadTabsPane();

    }

    public void removeTab(Tab tabToRemove, int index) {
        pages.remove(index);
        tabs.remove(tabToRemove);
        ui.reloadTabsPane();
    }

    public ObservableList<Tab> getTabs() {
        return tabs;
    }

    public ObservableList<Page> getPages() {
        return pages;
    }

    public EPortfolioMakerView getUi() {
        return ui;
    }
    

    public void reset() {
        tabs.clear();
        pages.clear();
    }

}
