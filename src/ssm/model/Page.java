/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssm.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.effect.InnerShadow;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import ssm.components.BannerImageComponent;
import ssm.components.Component;
import ssm.components.FooterComponent;
import ssm.components.StudentNameComponent;
import ssm.view.EPortfolioMakerView;

/**
 *
 * @author Randhawa
 */
public class Page {

    String pageTitle;
    VBox vbox;
    BannerImageComponent bannerImage; 
    StudentNameComponent studentName;
    FooterComponent footer;
    ObservableList<Component> components;
    Component selectedComponent;
    EPortfolioMakerView ui;
    HBox pageTitleBox;

    public Page(Tab tab, EPortfolioMakerView initUi) {
        ui = initUi;
        vbox = new VBox();
        pageTitle = tab.getText();
        pageTitleBox = new HBox();
        Label pageTitleLabel = new Label("Page Title: ");
        TextField pageTitleField = new TextField(pageTitle);
        pageTitleBox.getChildren().addAll(pageTitleLabel, pageTitleField);
        //vbox.getChildren().add(pageTitleBox);
        System.out.println("pageTitile " + pageTitle);
        components = FXCollections.observableArrayList();

        pageTitleField.textProperty().addListener((observable, oldValue, newValue) -> {
            pageTitle = newValue;
            tab.setText(newValue);
            //fileController.markAsEdited();
        });
    }

    public void addComponent(Component comp) {
        components.add(comp);
    }

    public void processComponentSelection(Component selectedComp) {
        if (selectedComp.getEffect()!=null) {
            selectedComp.editComponent();
        }
        //for (Component comp : components) {
        if (selectedComponent != null) {
            selectedComponent.setEffect(null);
        }
        //}

        applyEffects(selectedComp);
        selectedComponent = selectedComp;
        //ui.reloadTabsPane();
    }

    public void applyEffects(Component comp) {
        InnerShadow innerShadow = new InnerShadow();
        innerShadow.setOffsetX(7);
        innerShadow.setOffsetY(7);
        innerShadow.setColor(Color.web("7f0000"));

        comp.setEffect(innerShadow);
    }

    public void removeSelectedComponent(){
        components.remove(selectedComponent);
        ui.reloadTabsPane();
    }
    public String getPageTitle() {
        return pageTitle;
    }

    public ObservableList<Component> getComponents() {
        return components;
    }

    public VBox getVbox() {
        return vbox;
    }

    public HBox getPageTitleBox() {
        return pageTitleBox;
    }

    public BannerImageComponent getBannerImage() {
        return bannerImage;
    }

    public StudentNameComponent getStudentName() {
        return studentName;
    }

    
    public FooterComponent getFooter() {
        return footer;
    }

    public void setBannerImage(BannerImageComponent bannerImage) {
        this.bannerImage = bannerImage;
    }

    public void setStudentName(StudentNameComponent studentName) {
        this.studentName = studentName;
    }

    public void setFooter(FooterComponent footer) {
        this.footer = footer;
    }
    
    

    public Component getSelectedComponent() {
        return selectedComponent;
    }

}
