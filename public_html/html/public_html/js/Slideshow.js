/* 
 * @Author - Gauravdeep S Randhawa
 */

var playing;
 var bool = false;
 var imgArr = [REPLACE_IMG];
 var capArr = [REPLACE_CAP];
 var newTitle = "NEW_TITLE";
 var j;
 var playIcon = "img/play-icon.png";
 var pauseIcon = "img/pause-icon.png";
 var index = 0;
 var icon;
 
 /**
  * Set first image, caption and title right after the html is loaded
  * 
  */
 function firstFunc(){
     var image = document.getElementById('slideImage');
     image.src = imgArr[index];
     var cap = document.getElementById('caption');
     cap.innerHTML= capArr[index];
     document.title = newTitle;
     
 }
 /**
  * increase the variable index and set the next image and caption
  * @returns {undefined}
  */
 function nextImg() {
    index++;
    if(index>(imgArr.length-1))
        index = 0;

    var image = document.getElementById('slideImage');
    image.src = imgArr[index];
    var cap = document.getElementById('caption');
    cap.innerHTML= capArr[index];
}

/**
  * decrease the variable index and set the previous image and caption
  * @returns {undefined}
  */
function prevImg() {
    index--;
    if(index<0 ){
    index = imgArr.length-1;
}
    var image = document.getElementById('slideImage');
    image.src = imgArr[index];
    var cap = document.getElementById('caption');
    cap.innerHTML= capArr[index];
}

/**
 * controls the behavior of play slideshow button
 * when play button is clicked, variable bool is set to true, image is changed to pause,
 * nextImg() function is called after every 3 seconds with the use of setInterval method.
 * when pause button is clicked, the image is changed to play button, variable bool is set to false,
 * interval is cleared.
 * @returns {undefined}
 */
function setSlideShow(){
    if (bool) { 
       clearInterval(playing);
       icon = document.getElementById('playBtn');
       icon.src = playIcon;
       bool = false;
    }
    else {
       playing = setInterval(nextImg,3000);
       icon = document.getElementById('playBtn');
       icon.src = pauseIcon;
       bool = true;
    }
}
